#ifndef __PYTHONPROBLEM_H__
#define __PYTHONPROBLEM_H__

#include <memory>

#include "problem_interface.h"
#include "pymodule_wrapper.h"

#include "problem.h"

class TPythonProblem : public TProblem<TPythonProblem>
{
#undef OWNER_NAME
#define OWNER_NAME TPythonProblem
protected:

  int mDimension;
  bool mIsInitialized;
  std::string mPyFilePath;
  std::shared_ptr<TPythonModuleWrapper> mFunction;
  void* mLibpython_handle;

  virtual  void SetDefaultParameters();

public:
  TInt<TPythonProblem> temper_mode; //
  TDouble<TPythonProblem> percent;
  TString<TPythonProblem> pythonScriptFolder;


  TPythonProblem();

  virtual int SetDimension(int dimension);
  virtual int GetDimension() const;

  /// ������������� ����������
  virtual void Init(int argc, char* argv[], bool isMPIInit = false);
  /// �������� ������������ ��� ��������� ����������
  virtual int CheckValueParameters(int index = 0);

  //virtual int Initialize();

  virtual void GetBounds(double* upper, double *lower);
  virtual int GetOptimumValue(double& value) const;
  virtual int GetOptimumPoint(double* x) const;


  virtual double CalculateFunctionals(const double* x, int fNumber);

  /// ���������������� ������ ������ �� ���������� �������
  virtual void InitDataByParameters();

  ~TPythonProblem();
};

extern "C" LIB_EXPORT_API IProblem* create();
extern "C" LIB_EXPORT_API void destroy(IProblem* ptr);

#endif
// - end of file ----------------------------------------------------------------------------------
