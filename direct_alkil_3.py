import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

def ode_(times, init, parms):
    k_, const_count = parms

    x = np.array(init)

    xdot = np.zeros(x.size)
    xdot[0] = -k_[0] * x[0] + k_[1] * x[2] - k_[2] * x[0] * x[2] - \
              k_[6] * x[0] * x[1] * x[3] - k_[10] * x[0] + k_[13] * x[10]  # iC4H8 изобутелен?
    xdot[1] = -k_[3] * x[1] * x[3] - k_[5] * x[1] * x[4] - k_[6] * x[0] * x[1] * x[3] - \
              k_[14] * x[10] * x[1] * x[2]  # iC4 изобутан?
    xdot[2] = k_[0] * x[0] + k_[3] * x[1] * x[3] + k_[5] * x[1] * x[4] - \
              k_[2] * (x[0] + x[10]) * x[2] - k_[4] * x[11] * x[2] - k_[1] * x[2] + k_[6] * x[0] * x[1] * x[3] + \
              k_[14] * x[10] * x[1] * x[3]  # iC4+
    xdot[3] = k_[2] * (x[0] + x[10]) * x[2] - k_[3] * x[1] * x[3] - k_[6] * x[0] * x[1] * x[3] - \
              k_[14] * x[10] * x[1] * x[3]  # TMPs+
    xdot[4] = k_[4] * x[11] * x[2] - k_[5] * x[1] * x[4]  # DMHs+
    xdot[5] = k_[3] * x[1] * x[3]  # TMPs
    xdot[6] = k_[5] * x[1] * x[4] - k_[9] * x[6]  # DMHs
    xdot[7] = k_[6] * x[0] * x[1] * x[3] + k_[14] * x[10] * x[1] * x[3] + k_[8] * x[8] * x[9] - \
              k_[7] * x[7]  # HEs
    xdot[8] = k_[7] * x[7] - k_[8] * x[8] * x[9]  # iCx+
    xdot[9] = k_[7] * x[7] - k_[8] * x[8] * x[9]  # iCy=
    xdot[10] = -k_[2] * x[10] * x[2] - k_[14] * x[10] * x[1] * x[3] + k_[10] * x[0] + k_[11] * x[11] \
               - k_[12] * x[10] - k_[13] * x[10]  # 2-C4H8
    xdot[11] = -k_[4] * x[11] * x[2] + k_[12] * x[10] - k_[11] * x[11]  # 1-C4H8

    return xdot

def obj(p, tm):

    int_step = 0.1  # шаг интегрирования
    const_count = 15

    k = np.array(p)

    k[2] = k[2] * 10.0 ** 2.0  # k3
    k[4] = k[4] * 10.0 ** 4.0  # k5
    k[6] = k[6] * 10.0  # k7
    k[9] = k[9] / 10.0 ** 3.0  # k10
    k[11] = k[11] * 10.0 ** 2.0  # k12
    k[13] = k[13] * 10.0  # k14

    my_parms = [k, const_count]
    time = np.array([1, 2, 5, 10, 15, 20])

    temper_mode = tm  # from 1 to 4
    
    #print(k)
    #print(temper_mode)

    if temper_mode == 1:  # T = 276.2 K
        F = 0

        # exp1
        my_init = np.zeros(19)

        my_init[0] = 0.0142  # isobutene
        my_init[1] = 0.871  # isobutane
        my_init[10] = 0.0909  # cis + trans
        my_init[11] = 0.0239  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.54, 0.65, 0.69, 0.69, 0.7, 0.705])
        HE_exp = np.array([0.34, 0.24, 0.21, 0.21, 0.205, 0.205])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1


        # exp2
        my_init = np.zeros(19)

        my_init[0] = 0.0  # isobutene
        my_init[1] = 0.873  # isobutane
        my_init[10] = 0.0331 + 0.0599  # cis + trans
        my_init[11] = 0.034  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.58, 0.67, 0.7, 0.715, 0.725, 0.725])
        HE_exp = np.array([0.34, 0.24, 0.21, 0.2, 0.195, 0.195])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp3
        my_init = np.zeros(19)

        my_init[0] = 0.046  # isobutene
        my_init[1] = 0.866  # isobutane
        my_init[10] = 0.0314 + 0.0566  # cis + trans
        my_init[11] = 0.0  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.54, 0.6, 0.655, 0.67, 0.675, 0.68])
        HE_exp = np.array([0.36, 0.3, 0.25, 0.24, 0.24, 0.235])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                                method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

    elif temper_mode == 2:  # T = 279.2 K
        F = 0

        # exp1
        my_init = np.zeros(19)

        my_init[0] = 0.0142  # isobutene
        my_init[1] = 0.871  # isobutane
        my_init[10] = 0.0909  # cis + trans
        my_init[11] = 0.0239  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.52, 0.633, 0.68, 0.69, 0.695, 0.7])
        HE_exp = np.array([0.36, 0.262, 0.225, 0.22, 0.22, 0.22])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp2
        my_init = np.zeros(19)

        my_init[0] = 0.0  # isobutene
        my_init[1] = 0.873  # isobutane
        my_init[10] = 0.0331 + 0.0599  # cis + trans
        my_init[11] = 0.034  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.53, 0.63, 0.68, 0.695, 0.7, 0.71])
        HE_exp = np.array([0.37, 0.27, 0.22, 0.21, 0.21, 0.21])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp3
        my_init = np.zeros(19)

        my_init[0] = 0.046  # isobutene
        my_init[1] = 0.866  # isobutane
        my_init[10] = 0.0314 + 0.0566  # cis + trans
        my_init[11] = 0.0  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.5, 0.62, 0.625, 0.65, 0.66, 0.66])
        HE_exp = np.array([0.385, 0.27, 0.26, 0.24, 0.24, 0.24])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1
    elif temper_mode == 3:  # T = 282.2 K
        F = 0

        # exp1
        my_init = np.zeros(19)

        my_init[0] = 0.0142  # isobutene
        my_init[1] = 0.871  # isobutane
        my_init[10] = 0.0909  # cis + trans
        my_init[11] = 0.0239  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.53, 0.62, 0.67, 0.675, 0.68, 0.69])
        HE_exp = np.array([0.36, 0.275, 0.23, 0.225, 0.225, 0.22])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp2
        my_init = np.zeros(19)

        my_init[0] = 0.0  # isobutene
        my_init[1] = 0.873  # isobutane
        my_init[10] = 0.0331 + 0.0599  # cis + trans
        my_init[11] = 0.034  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.5, 0.59, 0.65, 0.665, 0.67, 0.67])
        HE_exp = np.array([0.39, 0.3, 0.25, 0.24, 0.24, 0.24])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp3
        my_init = np.zeros(19)

        my_init[0] = 0.046  # isobutene
        my_init[1] = 0.866  # isobutane
        my_init[10] = 0.0314 + 0.0566  # cis + trans
        my_init[11] = 0.0  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.5, 0.59, 0.62, 0.635, 0.645, 0.635])
        HE_exp = np.array([0.39, 0.3, 0.27, 0.26, 0.26, 0.26])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1
    else:  # 286.2 K
        F = 0

        # exp1
        my_init = np.zeros(19)

        my_init[0] = 0.0142  # isobutene
        my_init[1] = 0.871  # isobutane
        my_init[10] = 0.0909  # cis + trans
        my_init[11] = 0.0239  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.495, 0.59, 0.63, 0.64, 0.64, 0.65])
        HE_exp = np.array([0.375, 0.28, 0.27, 0.27, 0.26, 0.26])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp2
        my_init = np.zeros(19)

        my_init[0] = 0.0  # isobutene
        my_init[1] = 0.873  # isobutane
        my_init[10] = 0.0331 + 0.0599  # cis + trans
        my_init[11] = 0.034  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.48, 0.56, 0.605, 0.62, 0.635, 0.64])
        HE_exp = np.array([0.39, 0.31, 0.275, 0.27, 0.265, 0.26])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

        # exp3
        my_init = np.zeros(19)

        my_init[0] = 0.046  # isobutene
        my_init[1] = 0.866  # isobutane
        my_init[10] = 0.0314 + 0.0566  # cis + trans
        my_init[11] = 0.0  # 1-butene

        # DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
        TMP_exp = np.array([0.485, 0.56, 0.605, 0.62, 0.625, 0.625])
        HE_exp = np.array([0.395, 0.32, 0.275, 0.27, 0.27, 0.27])

        tspan = np.arange(0, 21, int_step)
        sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
                            method='Radau', y0=my_init, t_eval=tspan)

        output = sir_sol.y

        ind = time / int_step

        index = 0
        for i in ind:
            indi = int(i)
            F += abs(HE_exp[index] - output[7][indi] / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # HE
            F += abs(TMP_exp[index] - (output[3][indi] + output[5][indi]) / (
                    output[3][indi] + output[5][indi] + output[4][indi] + output[6][indi] + output[7][indi]))  # TMP
            index = index + 1

    return F


## решение (набор констант) из статьи:
##constants = np.array([16.6, 1.34, 3.04, 1.03, 0.63, 4.36, 9.99, 0.63, 4.52, 7.86, 7.95, 2.54, 7.13, 5.50, 32.62])
## func = obj(constants)
## print('func:')
#
#constants = np.array([6.609766, 1.652615, 2.036475, 0.997070, 0.610693, 1.705445, 15.983161, 0.430908, 6.253673,
#                      12.285723, 11.256527, 3.989502, 8.117041, 6.109082, 52.198943])
#
#
#func = obj(constants)
#print('func:')
#print(func)  # тут у меня печатает
## 1: 2.255177089636276
## 2: 2.7425103671180557
## 3: 3.388510367118055
## 4: 4.268510367118055
#
#
#int_step = 0.1  # шаг интегрирования
#const_count = 15
#
#constants[2] = constants[2] * 10.0 ** 2.0  # k3
#constants[4] = constants[4] * 10.0 ** 4.0   # k5
#constants[6] = constants[6] * 10.0  # k7
#constants[9] = constants[9] / 10.0 ** 3.0  # k10
#constants[11] = constants[11] * 10.0 ** 2.0  # k12
#constants[13] = constants[13] * 10.0  # k14
#
#temper = 3.55
#
#my_parms = [constants, const_count]
#
#fig, ax = plt.subplots()
#
#my_init = np.zeros(19)
#
#my_init[0] = 0.0142
#my_init[1] = 0.871
#my_init[10] = 0.0565 + 0.0344 #  0.0909
#my_init[11] = 0.0239
#
#time = np.array([1, 2, 5, 10, 15, 20])
#
#DMH_exp = np.array([0.09, 0.1, 0.1, 0.1, 0.1, 0.1])
#TMP_exp = np.array([0.56, 0.7, 0.76, 0.78, 0.78, 0.79])
#HE_exp = np.array([0.35, 0.25, 0.22, 0.22, 0.22, 0.22])
#
## x[5]: 2,2,4 - триметилпентан - 37.54
## x[6]: 2,3,3 - тмп - 21.69
## x[7]: 2,3,4 - тмп - 22.12
## x[8]: 2,3 - диметилгексан - 3.49
## x[9]: 2,4 - диметилгексан - 3.61
## x[10]: 2,5 - диметилгексан - 4.27
## x[11]: 2,4 - диметилпентан - 3.32
## x[12]: 2,3 - диметилпентан - 1.86
## x[13]: 2,2 - диметилпентан - 0.79
## x[14]: изодекан(2, 7 - диметилоктан) - 0.58
## x[15]: 2 - метилпентан - 0.80
## x[16]: 3 - метилгептан - 0.45
#
#
#
#
#tspan = np.arange(0, 21, int_step)
#sir_sol = solve_ivp(fun=lambda t, y: ode_(t, y, my_parms), t_span=[min(tspan), max(tspan)],
#                method='Radau', y0=my_init, t_eval=tspan)
#
#last = sir_sol.y.shape[1] - 1
#output = sir_sol.y
#
#output[4][0] = 4
#ax.plot(tspan, (output[3][:] + output[5][:])/(output[3][:] + output[5][:] + output[4][:] + output[6][:] + output[7][:]), label='TMPs', c='g')
#ax.plot(time, TMP_exp, 'go', marker='o', label='TMP_exp')
#output[4][0] = 0
#output[5][0] = 4
##ax.plot(tspan, (output[4][:] + output[6][:])/(output[3][:] + output[5][:] + output[4][:] + output[6][:] + output[7][:]), label='DMHs', c='r')
##ax.plot(time, DMH_exp, 'ro', marker='o', label='DMHs_exp')
#ax.plot(tspan, output[7][:]/(output[3][:] + output[5][:] + output[4][:] + output[6][:] + output[7][:]), label='HEs', c='b')
#ax.plot(time, HE_exp, 'bo', marker='o', label='HE_exp')
#
#
#plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
#           ncol=4, mode="expand", borderaxespad=0.)
#
#ind = time / int_step
#
#ax.grid()
#fig.savefig("test2.png")
#plt.show()