#pragma once

#include <string>
#include <vector>

#ifndef WIN32
  #ifdef _DEBUG
    #undef _DEBUG
    #include "Python.h"
    #define _DEBUG
  #else
    #include "Python.h"
  #endif
#else
  #ifdef _DEBUG
    #undef _DEBUG
    #include "python.h"
    #define _DEBUG
  #else
    #include "python.h"
  #endif
#endif


class TPythonModuleWrapper
{
protected:
  int mDimension;
  PyObject* mPFunc;
  std::vector<double> mUpperBound;
  std::vector<double> mLowerBound;

public:
  TPythonModuleWrapper(const std::string& module_path, int tm = 1);
  int GetDimension() const;
  void GetBounds(double* lower, double *upper) const;
  double EvaluateFunction(const double* y, int temper_mode) const;
  ~TPythonModuleWrapper();
};
