#include "pymodule_wrapper.h"

#include <stdexcept>
#include <iostream>


using namespace std;

PyObject* makeFloatList(const double* array, int size)
{
  PyObject *l = PyList_New(size);
  for (int i = 0; i != size; i++)
      PyList_SET_ITEM(l, i, PyFloat_FromDouble(array[i]));
  return l;
}

std::vector<double> floatListToVectorDouble(PyObject* incoming)
{
  std::vector<double> data;
  if (PyList_Check(incoming))
  {
    data.resize(PyList_Size(incoming));
    for(Py_ssize_t i = 0; i < PyList_Size(incoming); i++)
    {
      auto value = PyList_GetItem(incoming, i);
      data[i] = PyFloat_AsDouble(value);
      Py_DECREF(value);
    }
  }
  else
    throw std::logic_error("Passed PyObject pointer was not a list!");
  return data;
}

#ifdef WIN32
int setenv(const char* name, const char* value, int overwrite)
{
  int errcode = 0;
  if (!overwrite) {
    size_t envsize = 0;
    errcode = getenv_s(&envsize, NULL, 0, name);
    if (errcode || envsize) return errcode;
  }
  return _putenv_s(name, value);
}
#endif

TPythonModuleWrapper::TPythonModuleWrapper(const std::string& module_path, int tm)
{
  setenv("PYTHONPATH", module_path.c_str(), true);
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA1!!!!\n";
  Py_Initialize();
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA2!!!!\n";
  //auto pName = PyUnicode_FromString("objective");
  //auto pName = PyUnicode_FromString("direct_alkil_1");//���� � ��������
  //auto pName = PyUnicode_FromString("direct_alkil_2");//���� � ��������
  auto pName = PyUnicode_FromString("direct_alkil_3");//���� � ��������

  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA3!!!!\n";
  PyErr_Print();
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA4!!!!\n";  
  auto pModule = PyImport_Import(pName);
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA5!!!!\n";  
  if (pModule == nullptr)
  {
    PyErr_Print();
    std::exit(1);
  }
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA6!!!!\n";
  assert(pModule != nullptr);
  Py_DECREF(pName);
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA7!!!!\n";
  auto pDict = PyModule_GetDict(pModule);
  //std::cout << "AAAAAAAAAAAAAAAAAAAAAAAAAAAA8!!!!\n";
  //auto pDimension = PyDict_GetItemString(pDict, "dimension");
  //assert(pDimension != nullptr);
  //mDimension = PyLong_AsLong(pDimension);
  //Py_DECREF(pDimension);
  //assert(mDimension > 0 && mDimension < 50);
  mDimension = 15;

  //auto pUpper = PyDict_GetItemString(pDict, "upper");
  //assert(pUpper != nullptr);
  //mUpperBound = floatListToVectorDouble(pUpper);
  //Py_DECREF(pUpper);

  //auto pLower = PyDict_GetItemString(pDict, "lower");
  //assert(pLower != nullptr);
  //mLowerBound = floatListToVectorDouble(pLower);
  //Py_DECREF(pLower);

  mLowerBound.resize(mDimension);
  mUpperBound.resize(mDimension);
  
  // ��������� �����
  //vector<double> known_point = { 16.60, 1.34, 3.04, 1.03, 0.63, 4.36, 9.99, 0.63, 4.52, 7.86, 7.95, 2.54, 7.13, 5.5, 32.62 };
  //vector<double> known_point = { 6.609766, 1.652615, 2.036475, 0.997070, 0.610693, 1.705445, 15.983161, 0.430908, 6.253673, 12.285723, 11.256527, 3.989502, 8.117041, 6.109082, 52.198943 };
  //vector<double> known_point = { 6.609766, 1.652615, 2.036475, 0.997070, 0.610693, 1.705445, 15.983161, 0.430908, 6.253673, 12.285723, 11.256527, 3.989502, 8.117041, 6.109082, 52.198943 };
  //vector<double> known_point = { 3.767969, 0.767725, 3.184033, 0.414648, 0.374609, 1.808195, 22.028125, 0.684131, 8.824463, 9.078956, 16.727783, 5.380469, 12.679395, 5.193066, 60.201878 };//��� tm = 2
  //vector<double> known_point = { 5.64937576, 3.09018479, 3.95306805, 1.78588085, 0.36020577, 3.21886893, 30.17181003, 0.78455416, 12.22804026, 13.95678246, 21.90234228, 2.14584655, 11.8859951, 10.62835892, 53.63404183 };//��� tm=3
  vector<double> known_point = { 6.95519741, 3.80446374, 4.86679765, 2.19867723, 0.44346532, 3.9628925, 37.14585536, 0.96589948, 15.05448345, 17.18281476, 26.96494633, 2.64184699, 14.63337646, 13.0850447, 66.03125096 };//��� tm=4

  if (tm == 4)//��� temper_mode=4
  {
    known_point = { 6.95519741, 3.80446374, 4.86679765, 2.19867723, 0.44346532, 3.9628925, 37.14585536, 0.96589948, 15.05448345, 17.18281476, 26.96494633, 2.64184699, 14.63337646, 13.0850447, 66.03125096 };
  }
  else if (tm == 3)//��� temper_mode=3
  {
    known_point = { 5.64937576, 3.09018479, 3.95306805, 1.78588085, 0.36020577, 3.21886893, 30.17181003, 0.78455416, 12.22804026, 13.95678246, 21.90234228, 2.14584655, 11.8859951, 10.62835892, 53.63404183 };
  }
  else if (tm == 2)//��� temper_mode=2
  {
    known_point = { 3.767969, 0.767725, 3.184033, 0.414648, 0.374609, 1.808195, 22.028125, 0.684131, 8.824463, 9.078956, 16.727783, 5.380469, 12.679395, 5.193066, 60.201878 };//��� tm = 2
  }

  double percent = 0.6;
  for (int i = 0; i < known_point.size(); i++)
  {
      double ub = round(known_point[i] * (1 + percent) * 10) / 10;
      double lb = round(known_point[i] * (1 - percent) * 10) / 10;
      if (lb <= 0)
          lb = 0;

      mLowerBound[i] = lb;
      mUpperBound[i] = ub;
  }
  
// �������� ������� +- 20% �� ��������� ����� 
//  mLowerBound = { 13.3, 1.1, 2.4, 0.8, 0.5, 3.5, 8.0, 0.5, 3.6, 6.3, 6.4, 2.0, 5.7, 4.4, 26.1 };
//  mUpperBound = { 19.9, 1.6, 3.6, 1.2, 0.8, 5.2, 12.0, 0.8, 5.4, 9.4, 9.5, 3.0, 8.6, 6.6, 39.1 };

// �������� ������� +- 40% �� ��������� �����         
//  mLowerBound = { 10.0, 0.8, 1.8, 0.6, 0.4, 2.6, 6.0, 0.4, 2.7, 4.7, 4.8, 1.5, 4.3, 3.3, 19.6 };
//  mUpperBound = { 23.2, 1.9, 4.3, 1.4, 0.9, 6.1, 14.0, 0.9, 6.3, 11.0, 11.1, 3.6, 10.0, 7.7, 45.7 };

  
  if (tm == 1)// �������� ������� //��� temper_mode=1
  {
    mLowerBound = { 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001 };
    mUpperBound = { 1.937256, 0.326807, 2.188770, 0.315479, 0.162744, 1.958949, 14.149609, 1.049609, 13.390918, 14.499882, 24.262574, 7.671875, 19.412573, 3.864941, 74.638421 };
  }
  else if (tm == 3)// �������� ������� //��� temper_mode=3
  {    
    mLowerBound = { 1.937256, 0.326807, 2.188770, 0.315479, 0.162744, 1.958949, 14.149609, 1.049609, 13.390918, 14.499882, 24.262574, 7.671875, 19.412573, 3.864941, 74.638421 };
  }
  else if (tm == 4)//// �������� ������� //��� temper_mode=4
  {    
    mLowerBound = { 2.140724, 0.985543, 5.430780, 2.772541, 0.559221, 2.134611, 45.848776, 1.087632, 18.917850, 17.405883, 33.725979, 6.067836, 19.235094, 9.605116, 77.368866 };
  }

  std::cout << "\n mLowerBound: \n";
  for (int i = 0; i < mLowerBound.size(); i++)
  {
    std::cout << mLowerBound[i] << ", ";
  }

  std::cout << "\n mUpperBound: \n";
  for (int i = 0; i < mUpperBound.size(); i++)
  {
    std::cout << mUpperBound[i] << ", ";
  }

  std::cout << "\n" << std::endl;

  //mPFunc = PyDict_GetItemString(pDict, "objective");//�������
  mPFunc = PyDict_GetItemString(pDict, "obj");//�������
  Py_DECREF(pDict);
  assert(mPFunc != nullptr);
  assert(PyCallable_Check(mPFunc));
  Py_DECREF(pModule);
}

int TPythonModuleWrapper::GetDimension() const
{
  return mDimension;
}

double TPythonModuleWrapper::EvaluateFunction(const double* y, int temper_mode) const
{
  double retval = 0;
#pragma omp critical
{
  auto py_arg = makeFloatList(y, mDimension);
  auto tm = PyLong_FromLong(temper_mode);
  auto arglist = PyTuple_Pack(2, py_arg, tm);
  auto result = PyObject_CallObject(mPFunc, arglist);
  retval = PyFloat_AsDouble(result);
  Py_DECREF(py_arg);
  Py_DECREF(arglist);
  Py_DECREF(result);
}
  return retval;
}

TPythonModuleWrapper::~TPythonModuleWrapper()
{
  Py_DECREF(mPFunc);
  Py_Finalize();
}

void TPythonModuleWrapper::GetBounds(double* lower, double *upper) const
{
  for (int i = 0; i < mDimension; i++)
  {
    lower[i] = mLowerBound[i];
    upper[i] = mUpperBound[i];
  }
}
