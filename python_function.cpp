#ifndef WIN32
  #include <dlfcn.h>
#endif

#include <iostream>

#include "python_function.h"

void TPythonProblem::SetDefaultParameters()
{
  InitOption(temper_mode, 1, "-temper_mode", "temper_mode", 1);
  InitOption(percent, 0.6, "-percent", "Function number", 1);
  InitOption(pythonScriptFolder, python_objective_15D, "-pythonScriptFolder", "pythonScriptFolder", 1);
}

// ------------------------------------------------------------------------------------------------
TPythonProblem::TPythonProblem()
{
  mIsInitialized = false;
  mDimension = 15;
  mOwner = this;
  mMaxDimension = 15;
  mMinDimension = 15;
  mNumberOfConstraints = 0;
  mLeftBorder = -1.0;
  mRightBorder = 1.0;
}

// ------------------------------------------------------------------------------------------------
int TPythonProblem::SetDimension(int dimension)
{
    return IProblem::OK;
}

// ------------------------------------------------------------------------------------------------
int TPythonProblem::GetDimension() const
{
  return mDimension;
}

void TPythonProblem::Init(int argc, char* argv[], bool isMPIInit)
{
  mIsInit = false;
  TBaseProblem<TPythonProblem>::Init(argc, argv, false);
 
//  if (!mIsInitialized)
//  {
//#ifndef WIN32
//    mLibpython_handle = dlopen("/home/lebedev_i/miniconda3/lib/libpython3.9.so", RTLD_LAZY | RTLD_GLOBAL);
//#endif
//
//    std::string psf = pythonScriptFolder.ToString() + "t_" +    this->GetStringVal("ProcRank");
//    
//    std::cout << psf << "\n" << std::endl;
//
//    mFunction = std::make_shared<TPythonModuleWrapper>(psf);
//    mDimension = mFunction->GetDimension();
//
//    mIsInitialized = true;
//    return ;
//  }
//  else
//    return ;

  mIsInit = true;
}

int TPythonProblem::CheckValueParameters(int index)
{
  TBaseParameters<TPythonProblem>::CheckValueParameters(index);
  if (mIsInit)
  {
    if ((temper_mode > 4) || (temper_mode < 1))
      temper_mode = 1;
    if ((percent > 50) || (percent <= 0))
      percent = 0.6;

  }
  return 0;
}



// ------------------------------------------------------------------------------------------------
void TPythonProblem::GetBounds(double* lower, double *upper)
{
  if (mIsInitialized)
  {
    mFunction->GetBounds(lower, upper);
  }
}

// ------------------------------------------------------------------------------------------------
int TPythonProblem::GetOptimumValue(double& value) const
{
  value = 2.263900213975481;
  return IProblem::OK;
}

// ------------------------------------------------------------------------------------------------
int TPythonProblem::GetOptimumPoint(double* point) const
{
  point[0] = 16.60;
  point[1] = 1.34;
  point[2] = 3.04;
  point[3] = 1.03;
  point[4] = 0.63;
  point[5] = 4.36;
  point[6] = 9.99;
  point[7] = 0.63;
  point[8] = 4.52;
  point[9] = 7.86;
  point[10] = 7.95;
  point[11] = 2.54;
  point[12] = 7.13;
  point[13] = 5.5;
  point[14] = 32.62;

  return IProblem::OK;
}

// ------------------------------------------------------------------------------------------------
double TPythonProblem::CalculateFunctionals(const double* y, int fNumber)
{
  return mFunction->EvaluateFunction(y, temper_mode);
}

void TPythonProblem::InitDataByParameters()
{

  TBaseProblem<TPythonProblem>::InitDataByParameters();

  if (!mIsInitialized)
  {
#ifndef WIN32
    mLibpython_handle = dlopen("/home/lebedev_i/miniconda3/lib/libpython3.9.so", RTLD_LAZY | RTLD_GLOBAL);
#endif

    std::string psf = pythonScriptFolder.ToString() + "t_" + this->GetStringVal("ProcRank");

    std::cout << psf << "\n" << std::endl;
    int tm = temper_mode;
    mFunction = std::make_shared<TPythonModuleWrapper>(psf, tm);
    mDimension = mFunction->GetDimension();

    mIsInitialized = true;
    return;
  }
  else
    return;

}

// ------------------------------------------------------------------------------------------------
TPythonProblem::~TPythonProblem()
{
#ifndef WIN32
  if (mLibpython_handle)
    dlclose(mLibpython_handle);
#endif
}

// ------------------------------------------------------------------------------------------------
LIB_EXPORT_API IProblem* create()
{
  return new TPythonProblem();
}

// ------------------------------------------------------------------------------------------------
LIB_EXPORT_API void destroy(IProblem* ptr)
{
  delete ptr;
}
// - end of file ----------------------------------------------------------------------------------
