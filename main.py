# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import scipy.optimize as opt
import pandas as pd
import direct_alkil_3
import direct_alkil_E_k0

from scipy.optimize import Bounds

numFunCall = 0
numGradCall = 0
numHessCall = 0

def rosen(x):
    global numFunCall
    numFunCall += 1
    return np.sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0, axis=0)

def rosen_der (x):
    global numGradCall
    numGradCall += 1
    xm = x [1: -1]
    xm_m1 = x [: - 2]
    xm_p1 = x [2:]
    der = np.zeros_like (x)
    der [1: -1] = 200 * (xm-xm_m1 ** 2) - 400 * (xm_p1 - xm ** 2) * xm - 2 * (1-xm)
    der [0] = -400 * x [0] * (x [1] -x [0] ** 2) - 2 * (1-x [0])
    der [-1] = 200 * (x [-1] -x [-2] ** 2)
    return der

def rosen_hess(x):
    global numHessCall
    numHessCall += 1
    x = np.asarray(x)
    H = np.diag(-400*x[:-1],1) - np.diag(400*x[:-1],-1)
    diagonal = np.zeros_like(x)
    diagonal[0] = 1200*x[0]**2-400*x[1]+2
    diagonal[-1] = 200
    diagonal[1:-1] = 202 + 1200*x[1:-1]**2 - 400*x[2:]
    H = H + np.diag(diagonal)
    return H

def CalcCountFunCall(methodlist, accuracy, startPoint, n, fun, _bounds):
    global numFunCall
    global numGradCall
    global numHessCall
    numHessCall = 0
    methodName = []
    methodeCode = []
    numFunCalls = []
    numGradCalls = []
    numHessCalls = []
    summaryCall = []
    calcJac = []
    calcHess = []
    minimum = []
    funNum = []


    for method in methodlist:
        numFunCall = 0
        numGradCall = 0
        numHessCall = 0
        methodName.append(method[3])
        methodeCode.append(method[0])

        x0 = np.array(startPoint)
        if method[0] == 'bfgs':
            if method[1]:
                res = opt.minimize(fun, x0, method=method[0], jac=rosen_der, options={'eps': accuracy, 'disp': True}, bounds=_bounds)
            else:
                res = opt.minimize(fun, x0, method=method[0], options={'eps': accuracy, 'disp': True}, bounds=_bounds)
        if method[0] == 'l-bfgs-b':
            if method[1]:
                res = opt.minimize(fun, x0, jac=rosen_der, method=method[0], options={'eps': accuracy, 'disp': True}, bounds=_bounds)
            else:
                res = opt.minimize(fun, x0, method=method[0], options={'eps': accuracy, 'disp': True}, bounds=_bounds)

        if method[0] == 'newton-cg':
            if method[1]:
                if method[2]:
                    res = opt.minimize(fun, x0, method='newton-cg',
                                jac=rosen_der, hess=rosen_hess,
                                options={'xtol': accuracy, 'disp': True}, bounds=_bounds)
                else:
                    res = opt.minimize(fun, x0, method='newton-cg',
                                jac=rosen_der,
                                options={'xtol': accuracy, 'disp': True}, bounds=_bounds)
            else:
                if method[2]:
                    res = opt.minimize(fun, x0, method='newton-cg',
                                hess=rosen_hess,
                                options={'xtol': accuracy, 'disp': True}, bounds=_bounds)
                else:
                    res = opt.minimize(fun, x0, method='newton-cg',
                                options={'xtol': accuracy, 'disp': True}, bounds=_bounds)

        if method[0] == 'cg':
            res = opt.minimize(fun, x0, method=method[0], options={'eps': accuracy, 'disp': True}, bounds=_bounds)
        if method[0] == 'nelder-mead' or method[0] == 'powell' or method[0] == 'powell' or method[0] == 'tnc':
            res = opt.minimize(fun, x0, method=method[0], options={'xtol': accuracy, 'disp': True, 'maxiter': 10000}, bounds=_bounds)

        point = res.x
        val = res.fun
        numFunCall = res.nfev
        numGradCall = res.njev if hasattr(res, 'njev') else 0

        calcJac.append('True' if method[1] else 'False')
        calcHess.append('True' if method[2] else 'False')
        numFunCalls.append(numFunCall)
        numGradCalls.append(numGradCall)
        numHessCalls.append(numHessCall)
        summaryCall.append(numFunCall + numGradCall + numHessCall)
        #Формирование точки
        pointCoords = []
        for x in point:
            pointCoords.append((int)(x * 1000000) / 1000000)
        funNum.append((int)(val * 1000000) / 1000000)
        minimum.append(pointCoords)

        print("Метод ", method)
        print("Минимум найден в точке ", pointCoords)

    df = pd.DataFrame({
                           'Имя метода': methodName,
                           'Scipy метод': methodeCode,
                           'Оценки функции': numFunCalls,
                           'Оценки градиента': numGradCalls,
                           'Значение функции': funNum,
                           'Точка': minimum
                           })
    df.to_excel('./ExpN/LocalMethodTest_' + str(n) + 'D.xlsx', index=False)


"""
MINIMIZE_METHODS = ['nelder-mead', 'powell', 'cg', 'bfgs', 'newton-cg',
                    'l-bfgs-b', 'tnc', 'cobyla', 'slsqp', 'trust-constr',
                    'dogleg', 'trust-ncg', 'trust-exact', 'trust-krylov']
"""


if __name__ == '__main__':
    # Настраиваем 3D график
    """
    fig = plt.figure(figsize=[10, 10])
    ax = fig.gca(projection='3d')

    # Задаем угол обзора
    ax.view_init(45, 30)

    # Создаем данные для графика
    X = np.arange(-2, 2, 0.1)
    Y = np.arange(-1, 3, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = rosen(np.array([X, Y]))

    # Рисуем поверхность
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm)
    #plt.show()
    """

    experementList = [['l-bfgs-b', False, None, 'Лимитированный BFGS в многомерном кубе']]

    startPoint = [-1, -1]
    bounds = Bounds([-1, -1],
                    [3, 3])

    numFunCall = 0
    res = opt.minimize(rosen, startPoint, method='l-bfgs-b', options={'eps': 1e-5, 'disp': True},
                       bounds=bounds)

    print("F(",  res.x, ") = ", res.fun)
    print("Функция вызвана ", numFunCall, " раз")


    exit(0)

    for i in range(len(startPoint), 11):
        CalcCountFunCall(experementList, 1e-5, startPoint, i, rosen)
        startPoint.append(-2)




    experementList = [['nelder-mead', None, None, 'Нелдера-Мида'],
                      ['powell', None, None, 'Метод Пауэлла'],
                      ['cg', None, None, 'Метод сопряженных градиентов'],
                      ['bfgs', False, None, 'Бройдена-Флетчера-Голдфарба-Шанно (BFGS)'],
                      ['l-bfgs-b', False, None, 'Лимитированный BFGS в многомерном кубе']]
    """
    bounds = Bounds([1.5, 0.3, 1.3, 0.2, 0.1, 0.7, 8.8, 0.3, 3.5, 3.6, 6.7, 2.2, 5.1, 2.1, 24.1],
                    [6, 1.2, 5.1, 0.7, 0.6, 2.9, 35.2, 1.1, 14.1, 14.5, 26.8, 8.6, 20.3, 8.3, 96.3])


    fun = lambda x: direct_alkil_3.obj(x, 2)
    startPoint = [1.640076, 0.732971, 3.156860, 0.401355, 0.254236, 1.739251, 9.963379, 1.079395, 13.654681, 14.169167, 24.486243, 2.830744, 5.119513, 2.406519, 77.113062]
    CalcCountFunCall(experementList, 1e-5, startPoint, 15, fun, bounds)
    exit(0)
    



    experementList = [['l-bfgs-b', False, None, 'Лимитированный BFGS в многомерном кубе']]

    bounds = Bounds([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20, 100, 20,
                     100, 20,
                     100, 20, 100, 20, 100, 20])

    startPoint2 = [1.037598, 4.597559, 93.812207, 12.602441, 0.012207, 19.997559, 0.012207, 0.002441, 0.012207, 4.202441, 83.187793, 19.997559, 0.012207, 19.997559, 95.787793, 11.597559, 46.212207, 12.602441, 46.212207, 15.097559, 1.987793, 4.597559, 2.037793, 0.397559, 0.012207, 0.002441, 0.012207, 0.002441, 3.787793, 19.647559]
    fun2 = lambda x: direct_alkil_E_k0.obj(x)
    CalcCountFunCall(experementList, 1e-5, startPoint2, 30, fun2, bounds)
    """

